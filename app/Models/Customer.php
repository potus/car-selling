<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * 
 *
 * @property int $id
 * @property int $car_id
 * @property string $name
 * @property string $email
 * @property string $phone
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Car|null $car
 * @property-read mixed $car_name
 * @method static \Database\Factories\CustomerFactory factory($count = null, $state = [])
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Customer newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Customer newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Customer query()
 * @mixin \Eloquent
 */
class Customer extends Model
{
    use HasFactory;

    protected $guarded = false;

    protected $with = [
        'car.brand'
    ];

    protected $appends = [
        'car_name'
    ];

    public function car()
    {
        return $this->belongsTo(Car::class);
    }

    public function getCarNameAttribute()
    {
        return $this->car->name . '-' . $this->car->brand_name;
    }
}
