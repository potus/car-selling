<?php

namespace App\Http\Requests;

use App\Models\Car;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Database\Query\Builder;

class UpdateCustomerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'name' => 'required|string|max:255',
            'car_id' => [
                'required',
                Rule::exists(Car::class, 'id')->where(function (Builder $query) {
                    return $query->where('stock', '>', 0);
                })
            ],
            'phone' => 'required|string|max:255',
            'email' => 'required|email',
        ];
    }
}
