<?php

namespace App\Http\Controllers;

use App\Models\Car;
use Inertia\Inertia;
use App\Http\Requests\StoreCarRequest;
use App\Http\Requests\UpdateCarRequest;
use App\Models\Brand;

class CarController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return Inertia::render('Car/Index', [
            'cars' => Car::all()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return Inertia::render('Car/Form', [
            'brands' => Brand::all()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreCarRequest $request)
    {
        Car::create([
            'name' => $request->name,
            'brand_id' => $request->brand_id,
            'production_year' => $request->production_year,
            'price' => $request->price,
            'stock' => $request->stock,
            'photo' => $request->photo->store('cars', 'public'),
        ]);

        return to_route('cars.index');
    }

    /**
     * Display the specified resource.
     */
    public function show(Car $car)
    {
        return Inertia::render('Car/Show', $car);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Car $car)
    {
        return Inertia::render('Car/Form', [
            ...$car->toArray(),
            'brands' => Brand::all()
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateCarRequest $request, Car $car)
    {
        $car->update([
            'name' => $request->name,
            'brand_id' => $request->brand_id,
            'production_year' => $request->production_year,
            'price' => $request->price,
            'stock' => $request->stock,
            'photo' => $request->hasFile('photo') ? $request->photo->store('cars', 'public') : $car->photo,
        ]);

        return to_route('cars.index');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Car $car)
    {
        $car->delete();
    }
}
