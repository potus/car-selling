<?php

namespace App\Http\Controllers;

use App\Models\Car;
use App\Models\Customer;
use App\Http\Requests\StoreCustomerRequest;
use App\Http\Requests\UpdateCustomerRequest;
use Inertia\Inertia;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return Inertia::render('Customer/Index', [
            'customers' => Customer::all()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return Inertia::render('Customer/Form', [
            'cars' => Car::all()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreCustomerRequest $request)
    {
        Car::query()->where('id', $request->validated('car_id'))->decrement('stock');

        Customer::create($request->validated());

        return to_route('customers.index');
    }

    /**
     * Display the specified resource.
     */
    public function show(Customer $customer)
    {
        return Inertia::render('Customer/Form', [
            ...$customer->toArray(),
            'cars' => Car::all()
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Customer $customer)
    {
        return Inertia::render('Customer/Form', [
            ...$customer->toArray(),
            'cars' => Car::all()
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateCustomerRequest $request, Customer $customer)
    {
        if ($request->car_id !== $customer->car_id) {
            Car::query()->where('id', $customer->car_id)->increment('stock');
            Car::query()->where('id', $request->validated('car_id'))->decrement('stock');
        }

        $customer->update($request->validated());

        return to_route('customers.index');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Customer $customer)
    {
        Car::query()->where('id', $customer->car_id)->increment('stock');

        $customer->delete();
    }
}
