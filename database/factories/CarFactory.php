<?php

namespace Database\Factories;

use App\Models\Brand;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Car>
 */
class CarFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'brand_id' => Brand::factory(),
            'name' => Str::headline($this->faker->word()),
            'production_year' => $this->faker->year(),
            'price' => (int) $this->faker->numerify('##0000000'),
            'stock' => (int) $this->faker->numerify('##'),
        ];
    }
}
