export interface User {
    id: number;
    name: string;
    email: string;
    email_verified_at: string;
}

export interface Car {
    id: number;
    name: string;
    description: string;
    type: string;
    created_year: string;
}

export type PageProps<T extends Record<string, unknown> = Record<string, unknown>> = T & {
    auth: {
        user: User;
    };
};
